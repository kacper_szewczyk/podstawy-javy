package com.company;

public class Kalkulator {

    public static void dodawanie(long a, long b) {
        long result = a + b;
        System.out.println(a + " + " + b + " = " + result);
    }

    public static void dodawanie(float a, float b) {
        float result = a + b;
        System.out.println(a + " + " + b + " = " + result);
    }

    public void odejmowanie(int a, int b) {
        int result = a - b;
        System.out.println(a + " - " + b + " = " + result);
    }

    public void zwiekszOJeden(int a) {
        // przypisanie wyniku do nowej zmiennej
        int result = a + 1;
        showResultAndA(a, result);
        // zwiększenie wartości o jeden
        a = a + 1;
        showResultAndA(a, result);

        // zwiększenie wartości o jeden - krótszy zapis
        a += 1;
        showResultAndA(a, result);

        // zwiększanie wartosci zmiennej o jeden
        // najkrótszy zapis
        showResultAndA(a++, result);

        showResultAndA(++a, result);

    }

    private void showResultAndA(int a, int result) {
        System.out.println("result = " + result);
        System.out.println("a = " + a);
        System.out.println("--------");
    }
}
