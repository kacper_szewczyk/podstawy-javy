package com.company;

import dzien3.Book;

public class Warunki {

    public static void main(String[] args) {
        int zmienna = 1;
        switch (zmienna) {
            case 1:
                System.out.println("zmienna rowna 1");
                break;
            case 3:
                System.out.println("Zmienna rowna 3");
                break;
            default:
                System.out.println("Brak dopasowania");
                break;
        }
        zabawaZIfem();
        Book harryPotter = new Book();
    }

    private static void zabawaZIfem() {
        System.out.println("Początek programu");

        int a = 15;
        int b = 15;
        if (a < b) {
            System.out.println("a mniejsze od b");
        }
        else {
            System.out.println("a nie jest mniejsze od b");
        }

        if (a > b) {
            System.out.println("a większe od b");
        }

        if (a == b)
            System.out.println("a jest równe b");

        if (a != b) {
            System.out.println("a różne od b");
            System.out.println("a jest naprawdę różne od b");
        }

        System.out.println("Koniec programu");
    }
}
