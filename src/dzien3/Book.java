package dzien3;

public class Book {
    String author;
    public String title;
    private String isbn;
    protected String pageNumber;

    public void read() {
        System.out.println(title);
    }

    public Book() {
        title = "Lśnienie";
        isbn = "iosoadjoa1";
        pageNumber = "123";
    }

    public Book(String author, String title, String isbn, String pageNumber) {
        this.author = author;
        this.title = title;
        this.isbn = isbn;
        this.pageNumber = pageNumber;
    }

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
        this.isbn = "";
        this.pageNumber = "";
    }

    @Override
    public String toString() {
        String returnValue = "";
        if (checkIfNotEmpty(author)) {
            returnValue += "author='" + author + '\'' + ", ";
        }
        if (checkIfNotEmpty(title)) {
            returnValue += "title='" + title + '\'' + ", ";
        }
        if (checkIfNotEmpty(isbn)) {
            returnValue += "isbn='" + isbn + '\'' + ", ";
        }
        if (checkIfNotEmpty(pageNumber)) {
            returnValue += "pageNumber='" + pageNumber + '\'' + ", ";
        }
        return returnValue;
    }

    private boolean checkIfNotEmpty(String field) {
        return field != null && !field.isEmpty();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
}
