package dzien3.oop;

public class Kwadrat implements Figura {
    private double dlugoscBoku;

    public Kwadrat(double dlugoscBoku) {
        this.dlugoscBoku = dlugoscBoku;
    }

    @Override
    public double polePowierzchni() {
        return Math.pow(this.dlugoscBoku, 2);
    }

    @Override
    public double obwod() {
        return 4 * this.dlugoscBoku;
    }
}
