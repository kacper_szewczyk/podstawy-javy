package dzien3;

public class ZabawaZKlasami {
    public static void main(String[] args) {
        Book harryPotter = new Book();
        harryPotter.title = "Harry Potter";
        harryPotter.author = "J.K. Rowling";
        harryPotter.pageNumber = "123";
        harryPotter.read();

        Book domyslna = new Book();
        System.out.println(domyslna.toString());

        Book inicjalizowanyZParametrami = new Book("Kubuś puchatek", "Przygoda", "1231321", "222");
        System.out.println(inicjalizowanyZParametrami.toString());

        Book dwaParametry = new Book("Author", "Tytuł");
        System.out.println(dwaParametry.toString());
    }
}
