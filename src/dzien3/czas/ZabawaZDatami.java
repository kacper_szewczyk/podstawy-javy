package dzien3.czas;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class ZabawaZDatami {
    public static void main(String[] args) {
        localtimePlayground();
        localdatePlayground();
        LocalDateTime dataZGodzina = LocalDateTime.of(2018, 10, 4, 12, 0);
        System.out.println(dataZGodzina);

    }

    private static void localdatePlayground() {
        LocalDate aktualnaData = LocalDate.now();
        System.out.println(aktualnaData);
        String formattedDate = aktualnaData.getDayOfMonth() + "-"
                + aktualnaData.getMonth() + "-" + aktualnaData.getYear();
        System.out.println(formattedDate);
    }

    private static void localtimePlayground() {
        LocalTime localTime = LocalTime.now().withSecond(0).withNano(0);
        System.out.println(localTime);
        LocalTime twoHoursLater = localTime.plusHours(2);
        System.out.println(twoHoursLater);
        String formattedTime = twoHoursLater.getHour() + "." +
                twoHoursLater.getMinute() + "." + twoHoursLater.getSecond();
        System.out.println(formattedTime);

        System.out.println(Duration.between(localTime, twoHoursLater));
    }


}
