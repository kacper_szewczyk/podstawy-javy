package dzien4.oop;

public abstract class Figura {
    protected LiczbaBokow liczbaBokow;

    abstract double polePowierzchni();
    abstract double obwod();

    public String nazwaFigury() {
        return getClass().getSimpleName();
    }
}
