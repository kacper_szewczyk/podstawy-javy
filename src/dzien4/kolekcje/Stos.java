package dzien4.kolekcje;

import java.util.Stack;

public class Stos {
    public static void main(String[] args) {
        Stack<Integer> stos = new Stack<>();
        stos.push(1);
        stos.push(5);
        stos.push(10);
        for (Integer elem: stos) {
            System.out.println(elem);
        }
        while (!stos.empty()) {
            System.out.println(stos.pop());
        }
        System.out.println(stos.size());
    }
}
