package dzien4.kolekcje;

import java.util.TreeSet;

public class Zbior {
    public static void main(String[] args) {
        TreeSet<String> zbior = new TreeSet<>();
        zbior.add("John");
        zbior.add("John2");
        zbior.add("John");
        zbior.add("Paul");
        zbior.add("Adam");
        zbior.add("Kacper koadkoasda");
        System.out.println("Ilosc elementow w zbiorze: " + zbior.size());
        int index = 1;
        for (String imie : zbior) {
            System.out.println(index + ": " + imie);
            index++;
        }
        if (zbior.contains("Paul")) {
            System.out.println("Zawiera Paula");
        }
    }
}
