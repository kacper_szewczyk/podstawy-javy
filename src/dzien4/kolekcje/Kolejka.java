package dzien4.kolekcje;

import dzien4.oop.Trojkat;

import java.util.LinkedList;
import java.util.Queue;

public class Kolejka {
    public static void main(String[] args) {
        Queue<String> kolejka = new LinkedList<>();
        kolejka.add("John");
        kolejka.add("Henryk");
        kolejka.add("Donald Trump");
        while (!kolejka.isEmpty()) {
            System.out.println(kolejka.poll());
        }
        System.out.println("Ile zostało: " + kolejka.size());
        Trojkat trojkat = new Trojkat(1.1,1,1);

    }
}
