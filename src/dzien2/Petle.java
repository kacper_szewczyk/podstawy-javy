package dzien2;

public class Petle {

    public static void main(String[] args) {

        /*for (int i = 0; i < 5; i++) {
            System.out.println(i + ": jestem w petli");
        }
        System.out.println("jestem poza petla");

        powerOfNumber(5, 3);
*/
        int index = 10;
        while (index >= 0) {
            if (index != 0) {
                System.out.println(index);
            }
            else {
                System.out.println("BOOM");
            }
            index--;
        }

        do {
            if (index > 0) {
                System.out.println(index);
            }
            else {
                System.out.println("BOOM");
            }
            index--;
        } while (index >= 0);
    }

    private static void powerOfNumber(int liczbaKtoraChcemyPodniescDoPotegi, int potega) {
        int result = 1; // liczba do potęgi zerowej
        for (int i = 0; i < potega; i++) {
            result = result * liczbaKtoraChcemyPodniescDoPotegi; // result *= number;
            // ukryte jest to i++
        }
        System.out.println(result);
    }
}
