package dzien2;

public class Tablice {
    public static void main(String[] args) {
        int[] tablica = {1,5,6,2,5,2,3,5,10};

        wyswietlenieTablicyOdPierwszegoElementu(tablica);
        wyswietlenieTablicyOdOstatniegoElementu(tablica);

        for (int elem : tablica) {
            System.out.println(elem);
        }
        System.out.println("--------");
        int[] tablica2 = new int[10];
        for (int i = 0; i < tablica2.length; i++) {
            tablica2[i] = i;
        }
        wyswietlenieTablicyOdOstatniegoElementu(tablica2);


    }

    private static void wyswietlenieTablicyOdOstatniegoElementu(int[] tablica) {
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println(tablica[i]);
        }
        System.out.println("---------");
        System.out.println();
    }

    private static void wyswietlenieTablicyOdPierwszegoElementu(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
        System.out.println("---------");
        System.out.println();
    }
}
