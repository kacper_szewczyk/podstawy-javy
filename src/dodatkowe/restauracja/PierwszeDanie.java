package dodatkowe.restauracja;

public class PierwszeDanie extends Danie {
    public PierwszeDanie(String nazwa, Double cena) {
        super(nazwa, cena);
        System.out.println(
                "Konstruktor z klasy Pierwsze Danie");
    }

    @Override
    public void wyswietlOpis() {
        System.out.println("Zupa");
        super.wyswietlOpis();
    }
}
