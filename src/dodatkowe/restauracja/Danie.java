package dodatkowe.restauracja;

public class Danie {
    protected String nazwa;
    protected Double cena;

    public Danie(String nazwa, Double cena) {
        System.out.println("Konstruktor z klasy Danie");
        this.nazwa = nazwa;
        this.cena = cena;
    }

    public void wyswietlOpis() {
        System.out.println(nazwa
                + " - " + cena);
    }
}
