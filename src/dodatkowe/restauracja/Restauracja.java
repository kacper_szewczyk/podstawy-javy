package dodatkowe.restauracja;

public class Restauracja {

    public static void main(String[] args) {
        Danie danie =
                new Danie("Schabowy", 19.9);
        Danie pierwszeDanie =
                new PierwszeDanie("Rosół", 9.9);

        danie.wyswietlOpis();
        pierwszeDanie.wyswietlOpis();
    }
}
